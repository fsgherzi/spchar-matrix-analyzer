from scipy.io import mmread
from scipy.sparse import csr_matrix
import numpy as np
import math
import sys
import os
import threading

METRICS = ["branch_prediction_entropy(%)", "density(%)", "reuse_affinity(tl)", "index_affinity(sl)", *[f"row_imbalance_{i}" for i in [2, 4, 16, 32, 48, 64, 128]]]
HEADER = f"matrix,N,nnz,{','.join(METRICS)}"


def _max_entropy(N: int):
    acc = 0
    for _ in range(N):
        acc -= 1 / N * math.log(1 / N)

    return acc


def _entropy(indptr: list):
    N = (len(indptr) - 1)
    nnz_per_row = [0] * N
    values_to_probabilities = {}

    for i in range(N):
        nnz_per_row[i] = indptr[i + 1] - indptr[i]

    for row_count in nnz_per_row:
        if row_count not in values_to_probabilities:
            values_to_probabilities[row_count] = 0

        values_to_probabilities[row_count] += 1 / N

    acc = 0.0
    for k, v in values_to_probabilities.items():
        acc -= v * math.log(v)

    return acc


def branch_prediction_entropy(m: csr_matrix):
    """https://en.wikipedia.org/wiki/Entropy_(information_theory)"""
    max_entropy = _max_entropy(m.shape[0])
    r_entropy = _entropy(m.indptr)

    return r_entropy / max_entropy


def density(m: csr_matrix):
    nnz = len(m.data)
    N = m.shape[0]
    return nnz / N ** 2


def temporal_locality(m: csr_matrix):
    indices = set(m.indices)
    times_encountered = dict(zip(indices, [0] * len(indices)))
    last_time_encountered = dict(zip(indices, [0] * len(indices)))
    distance_encountered = dict(zip(indices, [0] * len(indices)))
    for i, cindex in enumerate(m.indices):
        times_encountered[cindex] += 1
        distance_encountered[cindex] += i - last_time_encountered[cindex]
        last_time_encountered[cindex] = i

    for cindex in indices:
        distance_encountered[cindex] /= times_encountered[cindex]

    acc = 0
    l_indices = len(indices)
    for k, v in distance_encountered.items():
        if times_encountered[k] != 1:
            acc += v

    if acc == 0:
        return np.inf

    return acc / l_indices


def spatial_locality(m: csr_matrix):
    # For every row, the average distance between elements
    nnz = len(m.indices)
    distances = [0] * nnz

    for i in range(nnz - 1):
        cur = m.indices[i]
        nxt = m.indices[i + 1]
        distances[i] = np.abs(nxt - cur)

    return np.mean(distances)


def _row_imbalance_for(cpu_cores: int, m: csr_matrix):
    nnz = len(m.indices)
    N = m.shape[0]
    ideal_nnz_per_core = nnz // cpu_cores
    pct_imbalance = 0.0
    nnz_on_core = [0] * cpu_cores
    rows_per_core = N // cpu_cores
    row_domain_per_core = []
    indptr = m.indptr

    for i in range(cpu_cores - 1):
        row_domain_per_core.append((i * rows_per_core, (i + 1) * rows_per_core))

    # Last core gets the remaining
    row_domain_per_core.append(((cpu_cores - 1) * rows_per_core, N))
    for core_idx, (begin, end) in enumerate(row_domain_per_core):
        for i in range(begin, end):
            nnz_on_core[core_idx] += (indptr[i + 1] - indptr[i])

    for nnz_core in nnz_on_core:
        pct_imbalance += np.abs(nnz_core - ideal_nnz_per_core) / ideal_nnz_per_core

    mean_imbalance = pct_imbalance / cpu_cores
    return mean_imbalance


def row_imbalance(m: csr_matrix):

    cpu_cores = [2, 4, 16, 32, 48, 64, 128]
    return [_row_imbalance_for(c, m) for c in cpu_cores]



def compute_matrix_metrics(matrix_path, out_file_path):
    if "row" not in matrix_path and "col" not in matrix_path:
        m = csr_matrix(np.transpose(mmread(matrix_path)))
    else:
        m = csr_matrix(mmread(matrix_path))

    open_mode = "a" if os.path.exists(out_file_path) else "w+"
    header = "" if os.path.exists(out_file_path) else HEADER + "\n"

    with open(out_file_path, open_mode) as out_file:
        out_file.write(header)
        N = m.shape[0]
        nnz = len(m.data)
        bp_entropy = branch_prediction_entropy(m)
        dens = density(m)
        ri = row_imbalance(m)
        tloc = 1 / math.log10(10 + temporal_locality(m)) ## Temporal affinity
        sloc = 1 / math.log10(10 + spatial_locality(m)) ## Spacial affinity
        data = [
            str(v) for v in [
                matrix_path, N, nnz, bp_entropy, dens, tloc, sloc, *ri
            ]
        ]
        out_file.write(",".join(data) + "\n")


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("[USAGE] ./matrix_metrics.py matrix_path out_csv_path")
        sys.exit(-1)

    compute_matrix_metrics(sys.argv[1], sys.argv[2])
