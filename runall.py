import os
import sys

from matrix_metrics import compute_matrix_metrics
from tqdm import tqdm
from multiprocessing import cpu_count, Process


def build_compute_matrix_metrics(out_file_path):
    return lambda matrix_path: compute_matrix_metrics(matrix_path, out_file_path)


def prepare_args(paths, out_file, threads):
    args = []
    chunk_size = len(paths) // threads
    if len(paths) > threads:
        for t_id in range(threads - 1):
            args.append(
                (paths[t_id * chunk_size: (t_id + 1) * chunk_size], out_file.replace(".csv", f"-{t_id}.csv"), t_id))
            print(len(args[-1][0]))

    args.append((
        paths[(threads - 1) * chunk_size:],
        out_file.replace(".csv", f"-{threads - 1}.csv"),
        threads - 1
    ))

    return args


def compute_matrix_metrics_multi(paths: list, out_file, thread_id):
    desc_str = f"[CORE] {thread_id}".ljust(15)
    for path in tqdm(paths, desc=desc_str, position=thread_id):
        compute_matrix_metrics(path, out_file)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("[USAGE] python runall.py matrix_dir outfile")

    base = sys.argv[1]
    outfile = sys.argv[2]
    threads = cpu_count()
    print(f"running on {threads} threads")
    paths = [f"{base}/{m}" for m in os.listdir(base)]
    args = prepare_args(paths, outfile, threads)
    processes = []

    for t_id in range(threads):
        processes.append(Process(target=compute_matrix_metrics_multi, args=args[t_id]))

    for pp in processes:
        pp.start()

    for pp in processes:
        pp.join()
