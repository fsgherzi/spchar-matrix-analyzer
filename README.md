# Matrix Analyzer

## Installation
Install the dependencies 
```bash
pip install -r requirements.txt
```

## Running 
```bash
python runall.py <matrix_directory> <base_output_file_name>
```
By default, this runs on all threads available in the system, and it creates an output file for each thread.